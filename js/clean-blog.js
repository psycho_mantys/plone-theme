// Floating label headings for the contact form
$(function() {
	$("body").on("input propertychange", ".floating-label-form-group", function(e) {
		$(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
	}).on("focus", ".floating-label-form-group", function() {
		$(this).addClass("floating-label-form-group-with-focus");
	}).on("blur", ".floating-label-form-group", function() {
		$(this).removeClass("floating-label-form-group-with-focus");
	});
});

$("form :input").each(function(index, elem) {
	var eId = $(elem).attr("id");
	var label = null;
	if (eId && (label = $(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
		$(elem).attr("placeholder", $.trim((label).html()).split('<span')[0]);
	}
});

$("input:checkbox,input:radio, input:file, input:hidden, select").parents('div').removeClass('floating-label-form-group');
$(".icon-controlpanel-errorLog").removeClass('alert alert-danger');

$(window).on("load", function() {
	// weave your magic here.
	$(".pattern-pickadate-date-wrapper").parents('div').removeClass('floating-label-form-group');
});

